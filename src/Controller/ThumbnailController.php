<?php

namespace App\Controller;

use App\Entity\Blend;
use App\Entity\Task;
use App\Entity\Tile;
use App\Repository\BlendRepository;
use App\Repository\FrameRepository;
use App\Repository\TaskRepository;
use App\Repository\TileRepository;
use App\Service\BlendService;
use App\Service\FrameService;
use App\Service\TileService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller to handle request for thumbnail
 * @Route("/thumb")
 */
class ThumbnailController extends AbstractController {

    /**
     * @Route("/{token}/{blend}/frame/{frameNumber}/thumbnail", methods="GET")
     */
    public function getFrameThumbnail(string $token, Blend $blend, int $frameNumber, BlendService $blendService, FrameRepository $frameRepository, FrameService $frameService, TaskRepository $taskRepository, EntityManagerInterface $entityManager) : BinaryFileResponse {
        if ($blendService->isThumbnailTokenValid($blend, $token) == false) {
            throw $this->createNotFoundException('token not valid');
        }
        $frame = $frameRepository->findOneBy(array('blend' => $blend->getId(), 'number' => $frameNumber));
        if (is_object($frame)) {
            $path = $frameRepository->getThumbnailPath($frame);

            if (file_exists($path) == false) {
                // don't exist -> create it and remove the waiting creation task
                if ($frameService->generateImageThumbnail($frame)) {
                    $tasks = $taskRepository->findBy(array('frame' => $frame, 'type' => Task::TYPE_GENERATE_FRAME_THUMBNAIL));
                    foreach ($tasks as $task) {
                        $entityManager->remove($task);
                        $entityManager->flush();
                    }
                }
            }

            return $this->giveFile($path);
        }
        else {
            throw $this->createNotFoundException('file not found');
        }
    }

    /**
     * @Route("/{token}/{blend}/tile/{tile}/thumbnail", methods="GET")
     */
    public function getTileThumbnail(string $token, Blend $blend, Tile $tile, BlendService $blendService, TileService $tileService, TileRepository $tileRepository, TaskRepository $taskRepository, EntityManagerInterface $entityManager) : BinaryFileResponse {
        if ($blendService->isThumbnailTokenValid($blend, $token) == false) {
            throw $this->createNotFoundException('token not valid');
        }
        $path = $tileRepository->getThumbnailPath($tile);

        if (file_exists($path) == false) {
            // don't exist -> create it and remove the waiting creation task
            if ($tileService->generateThumbnail($tile)) {
                $task = $taskRepository->findOneBy(array('tile' => $tile, 'type' => Task::TYPE_GENERATE_TILE_THUMBNAIL));
                if (is_object($task)) {
                    $entityManager->remove($task);
                    $entityManager->flush();
                }
            }
        }

        return $this->giveFile($path);
    }

    private function giveFile(string $path) {
        try {
            $file = new File($path, true);
        } catch (FileNotFoundException $e) {
            throw $this->createNotFoundException('file not found');
        }

        header('Content-Type: '.$file->getMimeType());
        header('Content-Length: '.filesize($file));

        return $this->file($file, $file->getFilename(), ResponseHeaderBag::DISPOSITION_INLINE);
    }
}