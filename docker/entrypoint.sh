#!/bin/bash
set -e

MAX_REQUEST_WORKERS=`cd /var/www/html/ && php bin/console shepherd:httpd-max-request`

sed -i -e "s/MaxRequestWorkers	  150/MaxRequestWorkers    $MAX_REQUEST_WORKERS/" /etc/apache2/mods-available/mpm_prefork.conf

if [ "$1" = 'apache2-foreground' ]; then
    service rsyslog start
    service cron start
fi

exec docker-php-entrypoint "$@"
