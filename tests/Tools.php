<?php


namespace App\Tests;


use App\Entity\Tile;
use App\Repository\FrameRepository;
use App\Repository\TileRepository;
use App\Service\BlendService;
use App\Service\FrameService;
use Doctrine\ORM\EntityManager;
use Imagick;
use ImagickPixel;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Tools {


    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var TileRepository
     */
    private $tileRepository;

    /**
     * @var FrameRepository
     */
    private $frameRepository;

    /**
     * @var FrameService
     */
    private $frameService;


    public function __construct(EntityManager $entityManager, TileRepository $tileRepository, FrameRepository $frameRepository, FrameService $frameService) {
        $this->entityManager = $entityManager;
        $this->tileRepository = $tileRepository;
        $this->frameRepository = $frameRepository;
        $this->frameService = $frameService;
    }

    public function generateFakeImage(int $width, int $height) : string {
        $tmpname = tempnam(sys_get_temp_dir(), 'img1');
        @unlink($tmpname);

        $path = $tmpname.'.png';

        $img = new Imagick();
        $img->newImage($width, $height, new ImagickPixel('red'));
        $img->setImageFormat('png');
        $img->writeImage($path);

        return $path;
    }


    public function validateTile(Tile $tile, UploadedFile $image) {
        $tile->setStatus(Tile::STATUS_FINISHED);
        $tile->setValidationTime(new \DateTime());

        $this->entityManager->persist($tile);
        $this->entityManager->flush();

        $root = $this->tileRepository->getStorageDirectory($tile);
        try {
//            echo __method__." move ".$root."/".$tile->getId() . '.' . $tile->getImageExtension()."\n";
            @mkdir($root);
            $image->move(
                $root,
                $tile->getId().'.'.$tile->getImageExtension());
        } catch (FileException $e) {
//            echo __method__.' '.$e."\n";
            return false;
        }

        if ($this->frameRepository->isFinished($tile->getFrame())) {
            $this->frameService->onFinish($tile->getFrame());
        }

        return true;
    }
}