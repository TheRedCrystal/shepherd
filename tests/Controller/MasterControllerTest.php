<?php


namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MasterControllerTest extends WebTestCase {

    public function testCreateBlendFull() {
        $client = static::createClient();
        $client->request(
            'POST',
            '/master/blend',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            '{ "blend": 63, "framerate": "24.0", "mp4": "1", "width": 1920, "height": 1080, "image_extension": "png", "frames": [ { "type": "full", "uid": "792", "number": "0001", "image_extension": "png", "token": "5e80b5bc462825.60064857" }, { "type": "full", "uid": "793", "number": "0002", "image_extension": "png", "token": "5e80b5bc462a05.39790910" }, { "type": "full", "uid": "794", "number": "0003", "image_extension": "png", "token": "5e80b5bc462b66.22116777" }, { "type": "full", "uid": "795", "number": "0004", "image_extension": "png", "token": "5e80b5bc462cc0.20562031" }, { "type": "full", "uid": "796", "number": "0005", "image_extension": "png", "token": "5e80b5bc462e16.24456896" }, { "type": "full", "uid": "797", "number": "0006", "image_extension": "png", "token": "5e80b5bc462f77.88489856" }, { "type": "full", "uid": "798", "number": "0007", "image_extension": "png", "token": "5e80b5bc4630c9.41744127" }, { "type": "full", "uid": "799", "number": "0008", "image_extension": "png", "token": "5e80b5bc463214.49318587" }, { "type": "full", "uid": "800", "number": "0009", "image_extension": "png", "token": "5e80b5bc463361.79562545" }, { "type": "full", "uid": "801", "number": "0010", "image_extension": "png", "token": "5e80b5bc4634b6.58967668" } ] }'
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testCreateBlendLayer() {
        $client = static::createClient();
        $client->request(
            'POST',
            '/master/blend',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            '{ "blend": 87, "framerate": "24.0", "width": 1920, "height": 1080, "mp4": "1", "image_extension": "png", "frames": [ { "type": "layer", "uid": 0, "number": "0001", "image_extension": "png", "tiles": [ { "type": "layer", "uid": "903", "number": "0000", "image_extension": "png" }, { "type": "layer", "uid": "904", "number": "0001", "image_extension": "png" }, { "type": "layer", "uid": "905", "number": "0002", "image_extension": "png" }, { "type": "layer", "uid": "906", "number": "0003", "image_extension": "png" } ] }, { "type": "layer", "uid": 0, "number": "0002", "image_extension": "png", "tiles": [ { "type": "layer", "uid": "907", "number": "0000", "image_extension": "png" }, { "type": "layer", "uid": "908", "number": "0001", "image_extension": "png" }, { "type": "layer", "uid": "909", "number": "0002", "image_extension": "png" }, { "type": "layer", "uid": "910", "number": "0003", "image_extension": "png" } ] }, { "type": "layer", "uid": 0, "number": "0003", "image_extension": "png", "tiles": [ { "type": "layer", "uid": "911", "number": "0000", "image_extension": "png" }, { "type": "layer", "uid": "912", "number": "0001", "image_extension": "png" }, { "type": "layer", "uid": "913", "number": "0002", "image_extension": "png" }, { "type": "layer", "uid": "914", "number": "0003", "image_extension": "png" } ] }, { "type": "layer", "uid": 0, "number": "0004", "image_extension": "png", "tiles": [ { "type": "layer", "uid": "915", "number": "0000", "image_extension": "png" }, { "type": "layer", "uid": "916", "number": "0001", "image_extension": "png" }, { "type": "layer", "uid": "917", "number": "0002", "image_extension": "png" }, { "type": "layer", "uid": "918", "number": "0003", "image_extension": "png" } ] }, { "type": "layer", "uid": 0, "number": "0005", "image_extension": "png", "tiles": [ { "type": "layer", "uid": "919", "number": "0000", "image_extension": "png" }, { "type": "layer", "uid": "920", "number": "0001", "image_extension": "png" }, { "type": "layer", "uid": "921", "number": "0002", "image_extension": "png" }, { "type": "layer", "uid": "922", "number": "0003", "image_extension": "png" } ] } ] }'
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testCreateBlendRegion() {
        $client = static::createClient();
        $client->request(
            'POST',
            '/master/blend',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],

            '{ "blend": 81, "framerate": "24.0", "width": 1920, "height": 1080, "mp4": "1", "image_extension": "png", "frames": [ { "type": "border", "uid": 0, "number": "0001", "image_extension": "png", "tiles": [ { "type": "border", "uid": "883", "number": "0000", "image_extension": "png" }, { "type": "border", "uid": "884", "number": "0001", "image_extension": "png" }, { "type": "border", "uid": "885", "number": "0002", "image_extension": "png" }, { "type": "border", "uid": "886", "number": "0003", "image_extension": "png" } ] }, { "type": "border", "uid": 0, "number": "0002", "image_extension": "png", "tiles": [ { "type": "border", "uid": "887", "number": "0000", "image_extension": "png" }, { "type": "border", "uid": "888", "number": "0001", "image_extension": "png" }, { "type": "border", "uid": "889", "number": "0002", "image_extension": "png" }, { "type": "border", "uid": "890", "number": "0003", "image_extension": "png" } ] }, { "type": "border", "uid": 0, "number": "0003", "image_extension": "png", "tiles": [ { "type": "border", "uid": "891", "number": "0000", "image_extension": "png" }, { "type": "border", "uid": "892", "number": "0001", "image_extension": "png" }, { "type": "border", "uid": "893", "number": "0002", "image_extension": "png" }, { "type": "border", "uid": "894", "number": "0003", "image_extension": "png" } ] }, { "type": "border", "uid": 0, "number": "0004", "image_extension": "png", "tiles": [ { "type": "border", "uid": "895", "number": "0000", "image_extension": "png" }, { "type": "border", "uid": "896", "number": "0001", "image_extension": "png" }, { "type": "border", "uid": "897", "number": "0002", "image_extension": "png" }, { "type": "border", "uid": "898", "number": "0003", "image_extension": "png" } ] }, { "type": "border", "uid": 0, "number": "0005", "image_extension": "png", "tiles": [ { "type": "border", "uid": "899", "number": "0000", "image_extension": "png" }, { "type": "border", "uid": "900", "number": "0001", "image_extension": "png" }, { "type": "border", "uid": "901", "number": "0002", "image_extension": "png" }, { "type": "border", "uid": "902", "number": "0003", "image_extension": "png" } ] } ] }'
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
