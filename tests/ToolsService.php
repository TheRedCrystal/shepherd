<?php


namespace App\Tests;


use App\Service\BlendService;

class ToolsService {
    /**
     * @var BlendService
     */
    private $blendService;

    /**
     * @var int
     */
    private $autoincrement;

    public function __construct(BlendService $blendService) {
        $this->blendService = $blendService;
        $this->autoincrement = 0;
    }

    public function addBlend(string $type, int $framesCount, int $tilesCount, int $width, int $height) {
        $id = rand(1000, 20000);

        $frames = array();

        for ($f = 0; $f < $framesCount; $f++) {
            $tiles = array();
            for ($t = 0; $t < $tilesCount; $t++) {
                $tiles [] = array('uid' => $this->autoincrement++, 'number' => $t, 'image_extension' => 'png');
            }
            $frames [] = array('type' => $type, 'uid' => uniqid(), 'number' => $f, 'image_extension' => 'png', 'tiles' => $tiles);

        }
        $this->blendService->addBlend($id, 'png', 25, $width, $height, true, $frames);

        return $id;
    }
}